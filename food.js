let lunch = [
    {item: "Steak Fajitas", price: 9.95},
    {item: "Chips and Guacamole", price: 5.25},
    {item: "Sweet Tea", price: 2.79}
];

let mySubtotal = getSubTotal(lunch);

let tax = Number((mySubtotal * 0.06)).toFixed(2);
let total = Number(mySubtotal) + Number(tax);

console.log ("Subtotal: "+ mySubtotal);
console.log ("Tax: "+ tax);
console.log ("Total: "+ total);

function getSubTotal (order) {
    let total = 0;

    for(let i = 0; i < order.length; i++) {
        total += order[i].price;
    }
    return Number(total);
}