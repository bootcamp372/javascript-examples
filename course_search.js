let courses = [
    {
    CourseId: "PROG100",
    Title: "Introduction to HTML/CSS/Git",
    Location: "Classroom 7",
    StartDate: "09/08/22",
    Fee: "100.00",
    },
    {
    CourseId: "PROG200",
    Title: "Introduction to JavaScript",
    Location: "Classroom 9",
    StartDate: "11/22/22",
    Fee: "350.00",
    },
    {
    CourseId: "PROG300",
    Title: "Introduction to Java",
    Location: "Classroom 1",
    StartDate: "01/09/23",
    Fee: "50.00",
    },
    {
    CourseId: "PROG400",
    Title: "Introduction to SQL and Databases",
    Location: "Classroom 7",
    StartDate: "03/16/23",
    Fee: "50.00",
    },
    {
    CourseId: "PROJ500",
    Title: "Introduction to Angular",
    Location: "Classroom 1",
    StartDate: "04/25/23",
    Fee: "50.00",
    }
];

    let startProg200 = getStartProg200(courses);
    console.log("Start Date for PROG200 is: " + startProg200);

    let titleProg500 = getTitletProg500(courses);
    console.log("Title for PROG500 is: " + titleProg500);

    let coursesUnder50 = getCoursesUnder50(courses);
    console.log("Courses under 50 include: ");
    printTitles(coursesUnder50);

    let classesinRoom1 = getClassesinRoom1(courses);
    console.log("Classes in Room 1 include: ");
    printTitles(classesinRoom1);

    
    function printTitles (obj) {
        for(let i = 0; i < coursesUnder50.length; i++) {
            console.log(coursesUnder50[i].Title);
        }
    }

    function getStartProg200 (courses) {

        // grab the Array item which matchs the CourseId "PROG200"
        let course = courses.find(course => course.CourseId === "PROG200");

        // return
        let startDate = course.StartDate;
        return startDate;

    }

    function getTitletProg500 (courses) {

        // grab the Array item which matchs the CourseId "PROG200"
        let matching = [];
        let numItems = courses.length;
        
        for(let i = 0; i < numItems; i++) {

        if (courses[i].CourseId == "PROJ500") {
            matching.push(courses[i]);
        }
        }
     
        let title = matching[0].Title;
        return title;
    }

    function getCoursesUnder50 (courses) {

        // grab the Array item which matchs the CourseId "PROG200"
        let matching = [];
        let numItems = courses.length;
        
        for(let i = 0; i < numItems; i++) {
            let courseFee = Number(courses[i].Fee)
        if (courseFee <= 50) {
            matching.push(courses[i]);
        }
        }
     
        return matching;
    }

    function getClassesinRoom1 (courses) {

        // grab the Array item which matchs the CourseId "PROG200"
        let matching = [];
        let numItems = courses.length;
        
        for(let i = 0; i < numItems; i++) {
            let location = courses[i].Location;
        if (location = "Classroom 1") {
            matching.push(courses[i]);
        }
        }
     
        return matching;
    }