// function greet() {
//     console.log("Hi!");
//   }

let f1 = (s1) => console.log(s1);
f1("Hello");

// function square(x) {
//     return x * x;
//   }

let f2 = x => x * x;
let square = f2(2);
console.log(square);

// function isPositive(x) {
//     return x >= 0;
//   }

let f3 = x => x >= 0;
let isPositive = f3(2);
console.log(isPositive);

// function subtract(x, y) {
//     return x - y;
//   }

let f4 = (x, y) => x - y;
//f4(2, 1);
let subtractVal = f4(2, 1);
console.log(subtractVal);

//   function biggestOfTwo(x, y) {
//     if (x > y) {
//       return x;
//     } else if (x < y) {
//       return y;
//     }
//   }

let f5 = (x, y) => {
    if (x > y) {
        return x;
    } else if (x < y) {
        return y;
    }
};
let biggestSum = f5(2, 1);
console.log(biggestSum);

//   function findBiggest(arr) {
//     let biggest = arr[0];
//     for (let i = 1; i < arr.length; i++) {
//       if (biggest < arr[1]) {
//         biggest = arr[1];
//       }
//     }
//     return biggest;
//   }

let f6 = (arr) => {
    let biggest = arr[0];
    for (let i = 1; i < arr.length; i++) {
        if (biggest < arr[i]) {
            biggest = arr[i];
        }
    }
    return biggest;
};
let findBiggest = f6([1,2,3,4,5]);
console.log(findBiggest);