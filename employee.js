class Employee {
    constructor(id, firstName, lastName, jobTitle, payRate) {
        this.employeeId = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.jobTitle = jobTitle;
        this.payRate = payRate;
    }
    getFullName() {
        return this.firstName + " " + this.lastName;
    }

    promote(newJobTitle, newPayRate) {
        this.jobTitle = newJobTitle;
        this.payRate = newPayRate;
    }

    getIntro() {
        let intro =
            "Hi! I'm " + this.getFullName() + " and I am a " +
            this.jobTitle;
        return intro;
    }
}

;
// Exercise 1
let employee1 = new Employee(1, "Sam", "Auston", "Graphic Artist", 42.50);
employee1.fullName = employee1.getFullName();
console.log(`Employee ${employee1.fullName} created`);
console.log(`Job title is ${employee1.jobTitle}`);
console.log(`Pay rate is $${employee1.payRate}`);
console.log(`---------------------------------`);

// Exercise 2
let employee2 = new Employee(
    1, "Frank", "Auston", "Graphic Artist", 42.50);
employee2.fullName = employee2.getFullName();
employee2.promote();
console.log(`Employee ${employee2.fullName} created`);
employee2.promote("Sr. Graphic Artist", 46.75);
console.log(`Job title is ${employee2.jobTitle}`);
console.log(`Pay rate is $${employee2.payRate}`);
console.log(`---------------------------------`);
// Exercise 3

let employee3 = new Employee(
    1, "Jack", "Auston", "Graphic Artist", 42.50);

let intro = employee3.getIntro();
console.log(intro);
employee3.promote("Sr. Graphic Artist", 46.75);
console.log(`Job title is ${employee3.jobTitle}`);
console.log(`Pay rate is $${employee3.payRate}`);
console.log(`---------------------------------`);