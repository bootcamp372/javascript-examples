"use strict"

window.onload = init;
function init() {
    const helloBtn = document.getElementById("helloBtn");
    helloBtn.onclick = onHelloBtnClicked;
    helloBtn.onmouseover = onHelloBtnMouseOver;
    helloBtn.onmouseout = onHelloBtnMouseOut;

}

function onHelloBtnClicked () {
    const para = document.getElementById("para");
    para.innerHTML = "onclick";
}

function onHelloBtnMouseOver() {
    const para = document.getElementById("para");
    para.innerHTML = "mouseover";
}

function onHelloBtnMouseOut() {
    const para = document.getElementById("para");
    para.innerHTML = "mouseout";
}