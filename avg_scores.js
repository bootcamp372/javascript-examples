let myScores = [92, 98, 84, 76, 89, 99, 100];
let yourScores = [82, 98, 94, 88, 92, 100, 100];

let myAverage = getAverage(myScores);
let yourAverage = getAverage(yourScores);

console.log("My Average: "+ myAverage);
console.log("Your Average: "+ yourAverage);

function getAverage(scores){
    // loop through, add all #s, divide by length of array
    let total = 0;

    for(let i = 0; i < scores.length; i++) {
        total += scores[i];
    }

    let average = total / scores.length;

    return average;
}