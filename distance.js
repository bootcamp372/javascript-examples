var distanceTotal = getDistance(0,0,100,100);
function getDistance (x1, y1, x2, y2) {
    // find the distance between (x1,y1) and (x2,y2)

    let y = x2 - x1;
    let x = y2 - y1;

    return Math.sqrt(Math.pow(x, 2) +  Math.pow(y, 2));

};

console.log(distanceTotal);