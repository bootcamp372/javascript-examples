let objSet1 = document.querySelectorAll("p");
let objSet2 = document.querySelectorAll(".attn");
let objSet3 = document.querySelectorAll("p.attn");
let objSet4 = document.querySelectorAll("img[alt]");
let objSet5 = document.querySelectorAll("div > p");
let objSet6 = document.querySelectorAll("div, span");

window.onload = init;

function init() {
    const imgGroups = document.getElementsByClassName("imageGroup");
    const imgs = document.querySelectorAll("img[alt=\"\"], img:not([alt])");

    for (let i = 0; i < imgGroups.length; i++) {
        imgGroups[i].style.border = "5px solid red";
    }

    // Array.from(imgGroups).forEach(function (element) {
    //     element.style.border = "2px solid red";
    //     });


    // for (let i = 0; i < imgs.length; i++) {
    //     imgs[i].alt = "graffiti image";
    // }

    // Array.from(imgs).forEach(function (element) {
    //     element.alt = "graffiti image";
    //     });

    Array.from(imgs).forEach((element) => {
        element.alt = "graffiti image";
    });

}