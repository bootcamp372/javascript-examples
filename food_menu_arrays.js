let menu = [
    [{
            item: "Sausage and Egg Biscuit",
            price: 3.69
        },
        {
            item: "Bacon and Egg Biscuit",
            price: 3.49
        },
        {
            item: "Ham and Egg Biscuit",
            price: 3.29
        },
        {
            item: "Egg and Cheese Biscuit",
            price: 2.29
        }
    ],
    [{
            item: "Soup and Sandwich",
            price: 5.69
        },
        {
            item: "Greek Salad",
            price: 6.49
        },
        {
            item: "Pizza Slices",
            price: 3.29
        },
        {
            item: "Burger and Fries",
            price: 5.29
        }
    ],
    [{
            item: "Chicken Dinner",
            price: 8.69
        },
        {
            item: "Eggplant Parmesan",
            price: 9.49
        },
        {
            item: "Lo Mein",
            price: 9.29
        },
        {
            item: "Spinach Florentine",
            price: 9.29
        },
        {
            item: "Steak and Potatoes",
            price: 12.29
        }
    ]
];

function getNumberBetween0and2(min, max) {
    return Math.round(Math.random() * (max - min) + min);
  }

let menuItem = getNumberBetween0and2(0, 2);
displayHeading(menuItem);

function displayHeading(menuItem) {
    console.log("Menu selection: " + menuItem);
    let heading = "";
    switch (menuItem) {
        case 0:
            heading = "Breakfast Menu";
            break;
        case 1:
            heading = "Lunch Menu";
            break;
        case 2:
            heading = "Dinner Menu";
            break;
        default:
            heading = "";
    }

    console.log("-----------");
    console.log(heading);
    console.log("-----------");
    console.log(menu[menuItem]);
}

