export class Person {
    constructor(firstName, lastName) {
        
        this.firstName = firstName;
        this.lastName = lastName;
    }
    getFullName() {
        return this.firstName + " " + this.lastName;
    }
}

class Employee extends Person {
    constructor(firstName, lastName, id, jobTitle, payRate) {
    super(firstName, lastName);
    this.id = id;
    this.jobTitle = jobTitle;
    this.payRate = payRate;
    }

    getFullName() {
        return this.firstName + " " + this.lastName;
    }

    getGrossPay(hoursWorked) {
        let grossPay = this.payRate * hoursWorked;
        return grossPay;
    }
    }

let person1 = new Person("John", "Smith");
person1.fullName = person1.getFullName();
console.log(`Person ${person1.fullName} created`);

let employee1 = new Employee("David", "Bowie", 2, "Musician", "50");
employee1.fullName = employee1.getFullName();
console.log(`Employee ${employee1.fullName} created`);
console.log(`-----------------`);
employee1.grossPay = employee1.getGrossPay(10);
console.log(`Employee ${employee1.fullName}'s gross pay is : ${employee1.grossPay}`);
console.log(`-----------------`);

let employee2 = new Employee("Tom", "Waits", 3, "Musician", "100");
employee2.fullName = employee2.getFullName();
console.log(`Employee ${employee2.fullName} created`);
console.log(`-----------------`);
employee2.grossPay = employee2.getGrossPay(40);
console.log(`Employee ${employee1.fullName}'s gross pay is :$ ${employee2.grossPay}`);
console.log(`-----------------`);