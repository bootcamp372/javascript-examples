parseAndDisplayName("Brenda Kaye");
parseAndDisplayName("Ian Auston");
parseAndDisplayName("Siddalee Grace");


function blankSpace(value){

    return value.indexOf(' ');
 }

function parseAndDisplayName(name) {
    const blankSpaceValue=blankSpace(name);

    let firstName = name.substring(0, blankSpaceValue);
    let secondName = name.substring(blankSpaceValue+1);
    
    
    console.log("First Name : " + firstName);
    console.log("Second Name : " + secondName);
}

