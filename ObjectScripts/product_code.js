// Product string in format: "supplierCode:productNumber-size"
let partCode1 = "12345ABCDE:678-large";
let part1 = parsePartCode(partCode1);
console.log(
    "Supplier: " + part1.supplierCode +
    " Product Number: " + part1.productNumber +
    " Size: " + part1.size
);

function parsePartCode(productString){
    
    const colonIndex = findColon(productString);
    const hyphenIndex = findHyphen(productString);

    let supplierCodeVal = productString.substring(0, colonIndex);
    let productNumberVal = productString.substring(colonIndex+1,hyphenIndex);
    let  sizeVal = productString.substring(hyphenIndex+1);

    let productObj = {
        supplierCode: supplierCodeVal,
        productNumber: productNumberVal,
        size: sizeVal
    };

    return productObj;

}

function findColon(value){

    return value.indexOf(':');
 }

 function findHyphen(value){

    return value.indexOf('-');
 }

 